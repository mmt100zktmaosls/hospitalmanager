package com.jmountain.hospitalmanager.interfaces;


public interface CommonModelBuilder<T> {
    T build();
}

