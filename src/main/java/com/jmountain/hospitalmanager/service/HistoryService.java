package com.jmountain.hospitalmanager.service;

import com.jmountain.hospitalmanager.entity.ClinicHistory;
import com.jmountain.hospitalmanager.entity.HospitalCustomer;
import com.jmountain.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.jmountain.hospitalmanager.model.HistoryItem;
import com.jmountain.hospitalmanager.model.HistoryRequest;
import com.jmountain.hospitalmanager.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory();
        addData.setHospitalCustomer(hospitalCustomer);
        addData.setMedicalItem(historyRequest.getMedicalItem());
        addData.setPrice(historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice());
        addData.setDateCure(LocalDate.now());
        addData.setTimeCure(LocalTime.now());
        addData.setIsCalculate(false);

        clinicHistoryRepository.save(addData);

    }
    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setHistoryId(item.getId());
            addItem.setCustomerId(item.getHospitalCustomer().getId());
            addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setPrice(item.getPrice());
            addItem.setIsSalaryName(item.getIsSalary() ? "예" : "아니요");
            addItem.setDateCure(item.getDateCure());
            addItem.setTimeCure(item.getTimeCure());
            addItem.setIsCalculate(item.getIsCalculate() ? "예" : "아니요");

            result.add((addItem));
        }
        return result;
    }
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(true, searchDate, true);

        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryInsuranceCorporationItem addItem = new HistoryInsuranceCorporationItem();
            addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setMedicalItemNonSalaryPrice(item.getMedicalItem().getNonSalaryPrice());
            addItem.setCustomerContributionPrice(item.getPrice());
            addItem.setBillingAmount(item.getMedicalItem().getNonSalaryPrice() - item.getPrice());
            addItem.setDateCure(item.getDateCure());

            result.add(addItem);
        }
        return result;
    }
}
