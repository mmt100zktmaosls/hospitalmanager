package com.jmountain.hospitalmanager.controller;

import com.jmountain.hospitalmanager.entity.HospitalCustomer;
import com.jmountain.hospitalmanager.model.HistoryItem;
import com.jmountain.hospitalmanager.model.HistoryRequest;
import com.jmountain.hospitalmanager.service.CustomerService;
import com.jmountain.hospitalmanager.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class HistoryController {

    private final CustomerService customerService;
    private final HistoryService historyService;

    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer, request);

        return "OK";
    }
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate")@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);
    }

}
