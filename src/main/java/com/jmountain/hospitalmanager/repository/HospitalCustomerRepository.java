package com.jmountain.hospitalmanager.repository;

import com.jmountain.hospitalmanager.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
